function [] = removeUpperTick(ax)
% Deletes upper Xtick and right Ytick.
% See https://stackoverflow.com/questions/15553720/matlab-remove-only-top-and-right-ticks-with-leaving-box-on

% set box property to off and remove background color
set(ax,'box','off','color','none')
% create new, empty axes with box but without ticks
bx = axes('Position',get(ax,'Position'),'box','on','xtick',[],'ytick',[]);
% set original axes as active
axes(ax);
% link axes in case of zooming
linkaxes([ax bx]);

end