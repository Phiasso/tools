function [] = makesquare(ax)
% MAKESQUARE
% MAKESQUARE(ax)
%
% Make axis square.

% --- Check input
if nargin < 1
    ax = gca;
end

ax.PlotBoxAspectRatio = [1 1 1];
ax.DataAspectRatioMode = 'auto';