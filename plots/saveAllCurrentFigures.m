function [] = saveAllCurrentFigures(savepath, varargin)
% Saves all current figures with their name + fig number in the specified
% path, in the specified format.
%
% INPUTS :
% ------
% savepath : prefix name of the file. The figure name is appended to that.
% 'fmt', 'svg' : file format ('svg', 'pdf', 'png', ...). Default is svg.
% 'overwrite' : overwrite file if it exists. Default is false.

% --- Check input
p = inputParser;
p.addRequired('savepath', @(x) ischar(x)||isstring(x));
p.addParameter('fmt', 'pdf', @(x) ischar(x)||isstring(x));
p.addParameter('overwrite', false, @islogical);
p.parse(savepath, varargin{:});

savepath = p.Results.savepath;
fmt = p.Results.fmt;
overwrite = p.Results.overwrite;

% --- Save figures
H = get(0, 'Children');

for idx_fig = 1:length(H)
    
    name = [savepath H(idx_fig).Name '_' num2str(H(idx_fig).Number) '.' fmt];
    
    if exist(name, 'file') && ~overwrite
        warning('File already exists, not saving.');
        continue
    else
        if verLessThan('matlab', '9.8') || strcmp(fmt, 'fig')
            saveas(H(idx_fig), name);
        else
            exportgraphics(H(idx_fig), name, 'ContentType', 'vector');
        end
    end
end

end