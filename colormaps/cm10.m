function map = cm10(m)
%CM10  Color map with 10 different colors.
%   
% Taken from :
% https://sashat.me/2017/01/11/list-of-20-simple-distinct-colors/
%
%   See also HSV, GRAY, PINK, COOL, BONE, COPPER, FLAG, 
%   COLORMAP, RGBPLOT.

%   Copyright 1984-2015 The MathWorks, Inc. 

if nargin < 1
   f = get(groot,'CurrentFigure');
   if isempty(f)
      m = size(get(groot,'DefaultFigureColormap'),1);
   else
      m = size(f.Colormap,1);
   end
end

c = [230, 25, 75;
    245, 130, 48;
    255, 255, 25;
    210, 245, 60;
    60, 180, 75;
    70, 240, 240;
    0, 130, 200;
    145, 30, 180;
    240, 50, 230;
    128, 128, 128]./255;

map = c(rem(0:m-1,size(c,1))+1,:);


