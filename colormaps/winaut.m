function map = winaut(m)
%WINAUT Winter-Autumn colormap
%   WINAUT(M) returns an M-by-3 matrix containing a colormap.
%
%   WINAUT returns a colormap with the same number of colors as the current
%   figure's colormap. If no figure exists, MATLAB uses the length of the
%   default colormap.
%
%   EXAMPLE
%
%   This example shows how to reset the colormap of the current figure.
%
%       colormap(WINAUT)
%
%   See also AUTUMN, BONE, COLORCUBE, COOL, COPPER, FLAG, GRAY, HOT, HSV,
%   JET, LINES, PINK, PRISM, SPRING, SUMMER, WHITE, WINTER, COLORMAP,
%   RGBPLOT.

if nargin < 1
    f = get(groot,'CurrentFigure');
    if isempty(f)
        m = size(get(groot,'DefaultFigureColormap'),1);
    else
        m = size(f.Colormap,1);
    end
end

aut = autumn(170);
win = winter(170);
aut = aut(1:128, :);
win = win(1:128, :);

winaut_data = [win ; aut];

P = size(winaut_data,1);
map = interp1(1:size(winaut_data,1), winaut_data, linspace(1,P,m), 'linear');