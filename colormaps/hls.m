function map = hls(m)
%HLS  Color map with the line colors.
%   HLS(M) returns an M-by-3 matrix containing a HLS
%   colormap. HLS, by itself, is the same length as the current
%   colormap. If no figure exists, MATLAB uses the length of the
%   default colormap.
%
%   Values taken from the HLS colormap from seaborn, see :
%   http://seaborn.pydata.org/tutorial/color_palettes.html
%
%   For example, to set the colormap of the current figure:
%
%       colormap(hls)
%
%   See also HSV, GRAY, PINK, COOL, BONE, COPPER, FLAG, 
%   COLORMAP, RGBPLOT.

if nargin < 1
   f = get(groot,'CurrentFigure');
   if isempty(f)
      m = size(get(groot,'DefaultFigureColormap'),1);
   else
      m = size(f.Colormap,1);
   end
end

hls_data = [219, 94, 86;
219, 174, 86;
184, 219, 86;
105, 219, 86;
86, 219, 147;
86, 211, 219;
86, 131, 219;
121, 86, 219;
200, 86, 219;
219, 86, 158];

hls_data = hls_data./255;

map = hls_data(rem(0:m-1,size(hls_data,1))+1,:);