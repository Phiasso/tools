function [mirpoint] = mirrorLine(point, pline1, pline2)
% [mirrorx, mirrory] = MIRRORLINE(point, pline1, pline2)
%
% Compute the coordinates mirpoint, mirror of coordinates specified by
% point with respect to the line defined by the two points pline1 and
% pline2.
%
% INPUTS :
% ------
% point : (x, y) coordinates of the point to mirror.
% pline 1 : (x, y) coordinates of one point of the segment
% pline 2 : (x, y) coorsinates of one point of the segment
%
% RETURNS :
% -------
% mirpoint : (x, y) coordinates of the mirrored point.

% Gather coordinates
px = point(1);
py = point(2);
x1 = pline1(1);
x2 = pline2(1);
y1 = pline1(2);
y2 = pline2(2);

% Get line equation Ax + Ay + C = 0
A = y2 - y1;
B = -(x2 - x1);
C = -A*x1 - B*y1;

% Normalize equation to get unit vectors
M = sqrt(A*A + B*B);
An = A/M;
Bn = B/M;
Cn = C/M;

% Get signed distance between the point and the line
D = An*px + Bn*py + Cn;

% Translate point coordinates of 2D against unit vector
pxm = px - 2*An*D;
pym = py - 2*Bn*D;
mirpoint = [pxm, pym];