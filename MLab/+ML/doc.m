function doc(varargin)
% ML.doc Reference page in MLab help browser
%   ML.doc('ok') This is bla bla.

% --- Inputs --------------------------------------------------------------

in = ML.Input;
in.file = @(x) ischar(x) || ML.isfunction_handle(x);
in = +in;

% -------------------------------------------------------------------------

clc

% --- Checks

% Convert in.file to string
if ML.isfunction_handle(in.file)
    in.file = func2str(in.file);
end

% Add 'ML.' at the beginning of in.file
if numel(in.file)<4 || ~strcmp(in.file(1:3), 'ML.')
    in.file = ['ML.' in.file];
end

% --- Locate file
fname = which(in.file);

% --- Parse file
fid = fopen(fname);
tmp = textscan(fid, '%s', 'Delimiter', '\n');
fclose(fid);

C = tmp{1};
S = struct();
cs = '';

for i = 1:numel(C)
    
    % Remove trailing spaces
    line = deblank(C{i});
    
    % Comments
    if numel(line)<2 || numel(line)>7 && strcmp(line(1:8), '%! -----')
        continue
    end
        
    % New section
    if numel(line)>3 && strcmp(line(1:3), '%! ')
        
        tmp = regexp(line(4:end), '([^:]*):?(.*)', 'tokens');
        section = tmp{1}{1};
        if ~isempty(tmp{1}{2})
            S.(section) = tmp{1}{2};
            cs = '';
        else
            cs = section;
        end
        continue
    end
    
    % Current section
    if ~isempty(cs)
        if ~isfield(S, cs)
            S.(cs) = strtrim(line(2:end));
        else
            S.(cs) = [S.(cs) char(10) strtrim(line(2:end))];
        end
    end
end

% --- Prepare HTML
html = ML.Doc.html_R2014a(S.Doc, 'title', in.file);

% --- Display html
% fprintf(html);
% fprintf('\n');
% return

% --- Browser display
tmp = tempname;
fid = fopen(tmp, 'w');
fprintf(fid, html);
fclose(fid);
web(tmp, '-notoolbar');
% web(tmp, '-new', '-notoolbar');

% -------------------------------------------------------------------------
    function add(txt)
        html = [html char(10) txt];
    end
end