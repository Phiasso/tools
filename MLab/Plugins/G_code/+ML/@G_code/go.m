function go(this, varargin)
%GEOM.PATTERNS.G_CODE.GO moves tool to a position at a certain velocity
%*  G_CODE.GO(X,Y,Z,V) moves the tool to position (X,Y,Z) at velocity V.
%
%*  G_CODE.GO(X,Y,Z,V,COM) adds a comment.
%
%*  See also: Geom.patterns.G_code.

% === Input variables =====================================================

in = inputParser;
in.addOptional('x', NaN, @isnumeric);
in.addOptional('y', NaN, @isnumeric);
in.addOptional('z', NaN, @(x) isnumeric(x)|ischar(x));
in.addParamValue('v', NaN, @(x) isnumeric(x)|ischar(x));
in.addParamValue('i', Inf, @isnumeric);
in.addParamValue('j', Inf, @isnumeric);
in.addParamValue('o', NaN, @isnumeric);
in.addParamValue('com', '', @ischar);

in.parse(varargin{:});
in = in.Results;

% =========================================================================

% --- Special cases
switch in.z
    case 'clear'
        in.z = max(this.tools(this.tool).diam, this.substrate.z_clear);
    case 'up'
        in.z = this.substrate.z_up;
end

if ischar(in.v)
    in.v = this.tools(this.tool).feedrates.(in.v);     % Far, close, in
end

% --- Get last position
last = this.trajs(this.tool).traj(end,:);

% --- X
if ~isnan(in.x)
    dx2 = (last(1) - in.x)^2;
else
    dx2 = 0;
    in.x = last(1);
end

% --- Y
if ~isnan(in.y)
    dy2 = (last(2) - in.y)^2;
else
    dy2 = 0;
    in.y = last(2);
end

% --- Z
if ~isnan(in.z)
    dz2 = (last(3) - in.z)^2;
else
    dz2 = 0;
    in.z = last(3);
end

% --- V
if isnan(in.v)
    in.v = last(4);
end

% --- Comments

if ~isempty(in.com)
    
    % New lines gestion  
    in.com = strrep(in.com, '\t', [char(9)]);
    in.com = strrep(in.com, '\n', [')' char(10) '(']);
    in.com = strrep(in.com, '()', '');
    
end

% --- Compute time
dr = sqrt(dx2 + dy2 + dz2);
if ~isnan(dr) & ~isnan(in.v)
    this.trajs(this.tool).time = this.trajs(this.tool).time + dr/in.v*60;
end

% --- Actualize trajectory

this.trajs(this.tool).traj(end+1,:) = [in.x in.y in.z in.v in.i in.j in.o];
this.trajs(this.tool).coms{end+1} = in.com;

