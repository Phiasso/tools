function times(this, tools)
%GEOM.PATTERNS.G_CODE.TIMES Display total times for each tool
%*  G_CODE.TIMES() display the total machining time for each tool.
%
%*  See also: Geom.Patterns.G_code.

% --- Default values
if ~exist('tools', 'var')
    tools = 1:numel(this.tools); 
end

% --- Generate trajectories (optional)
for i = tools
    if numel(this.trajs)<i || isempty(this.trajs(i).traj)
        this.generate(i);
    end
end

% --- Display
for i = 1:numel(this.tools)
    tmp = ML.Time.s2h(this.trajs(i).time);
    fprintf('Tool #%i: %s\n', i, tmp.str);
end