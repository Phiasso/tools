function save(this, D, varargin)
%GEOM.PATTERNS.G_CODE.SAVE saves G-codes to a set o files
%*  G_CODE.SAVE(DIR) save the G-codes in files in the directory DIR. A file
%   per tool is written.
%
%*  See also: Geom.Patterns.G_code.

% === Input variables =====================================================

in = inputParser;
in.addRequired('D', @ischar);
in.addParamValue('create', true, @islogical);

in.parse(D, varargin{:});
in = in.Results;

% =========================================================================

% --- Base dir
D = in.D;
if ~strcmp(D(end), filesep)
    D(end+1) = filesep;
end

if numel(this.title), D = [D this.title filesep()]; end

for i = 1:numel(this.tools)
    
    % --- Checks
    if isempty(this.trajs(i).code)
        warning('G_CODE:Save', 'No G-code to save.');
        continue;
    end
    
    % Dir existence
    if ~exist(D, 'dir')
        if in.create
            mkdir(D);
        else
            error('G_CODE:Save', ['The directory ' D ' do not exist.']);
        end
    end
    
    % --- Save
    fname =  [D 'Tool_' num2str(i) '.gco'];
    fid = fopen(fname, 'w');
    fprintf(fid, '%s', this.trajs(i).code);
    fclose(fid);
    
end