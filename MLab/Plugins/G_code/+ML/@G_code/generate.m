function generate(this, tools)
%GEOM.PATTERNS.G_CODE.GENERATE Generates the trajectories
%*  T = G_CODE.GENERATE() Generates the trajectories of the different tools.
%   T is a struct, each element standing for a tool. For each element, the
%   fields are:
%   * 'tool': the tool to use
%   * 'traj': the trajectory of the tool [x y z v] (in mm and mm/min)
%
%*  See also: Geom.patterns.G_code.

% --- Default values
if ~exist('tools', 'var')
    tools = 1:numel(this.tools);
end

% --- Store current tool
ctool = this.tool;

% --- Process
for i = tools
    
    % Tool to use
    this.tool = i;
    this.trajs(i).tool = this.tools(i);
    % tool = this.tools(i);
    
    % Initialization
    this.trajs(i).traj = NaN(1,7);
    this.trajs(i).coms = {''};
    this.trajs(i).time = 0;
    
    switch this.trajs(i).tool.type
        
        case 'drill'
            
            % With drills, the hole pattern is globalized and the
            % trajectory is optimized for all the holes at once.
            
            % Holes are [x y z z_step z_start]
            H = NaN(0,5);
            
            for j = 1:numel(this.elms)
                
                % -- Define element
                elm = this.elms{j};
                
                % -- Checks
                
                % Tool
                if this.elms{j}.tool~=i, continue; end
                
                % Element type
                if ~strcmp(elm.type, 'hole')
                    warning('G_CODE:Generate', 'Trying to use a drill for a patterns which is not a hole.');
                    continue
                end
                
                % Depth
                if elm.z>=0
                    warning('G_CODE:Generate', 'Trying drill a hole above 0 ?');
                end
                
                H(end+1,:) = [elm.pos elm.z elm.z_step elm.z_start];
                
            end
                       
            % -- Define trajectory
                        
            % Optimize path
            if this.tools(this.elms{j}.tool).optimize
                P = ML.G_code.Path(H(:,1:2));
                J = P.optimize();
            else
                J = 1:size(H,1);
            end
                        
            % Define trajectory
            for j = 1:numel(J)
                
                this.com(['~~~ Hole [' num2str(H(J(j),1)) ', ' ...
                                       num2str(H(J(j),2)) ', ' ...
                                       num2str(H(J(j),3)) '] ~~~']);
                                   
                % - Position
                this.go(H(J(j),1), H(J(j),2), NaN, 'v', 'far', 'com', '>.<');
                
                p = this.pos(i);
                if isnan(p(3)) || p(3)>this.substrate.z_clear
                    this.go(NaN, NaN, 'clear', 'v','far', 'com', '-V-');
                end
                
                % - Recursive drilling
                for k = 1:ceil(abs((H(J(j),3)-H(J(j),5))/H(J(j),4)))
                    this.go(NaN, NaN, max(H(J(j),5)-k*H(J(j),4), H(J(j),3)), 'v', 'in', 'com', '-v-');
                    this.go(NaN, NaN, 'clear', 'v', 'close', 'com', '-^-');
                end
                
                % - Long run ?
                if j<numel(J) & sum((H(J(j),1:3)-H(J(j+1),1:3)).^2)>=this.substrate.d_up^2
                    this.go(NaN, NaN, 'up', 'v', 'far', 'com', '_^_'); 
                end
                
            end
            
        case 'mill'
            
            for j = 1:numel(this.elms)
                
                % --- Get element
                elm = this.elms{j};
                
                % --- Checks
                
                % Tool
                if this.elms{j}.tool~=i, continue; end
                
                % Depth
                z_min = min(elm.traj(:,3));
                if z_min>0
                    warning('G_CODE:Generate', 'Trying mill a trajectory above 0.');
                else
                    n_step = max(1,ceil(abs((z_min-elm.z_start)/elm.z_step)));
                end
                
                % --- Initial position
                this.go(elm.traj(1,1),elm.traj(1,2), NaN, 'v', 'far', 'com', '>.<');
                this.go(NaN, NaN, 'clear', 'v', 'far', 'com', '-V-');
                
                for k = 1:n_step
                    
                    % Forward or reverse ?
                    if mod(k,2), t = elm.traj;
                    else
                        t = flipud(elm.traj);
                        t(:,6) = -t(:,6);
                        t(:,4:6) = [t(end,4:6) ; t(1:end-1,4:6)];
                    end
                    
                    for l = 1:size(t,1)
                        this.go(t(l,1), t(l,2), max(t(l,3), elm.z_start-k*elm.z_step), 'v', 'in', ...
                                'i', t(l,4), 'j', t(l,5), 'o', t(l,6), 'com', '~x~');
                    end
                    
                end
                
                % - Clear at the end of the traj
                this.go(NaN, NaN, 'up', 'v', 'far', 'com', '_^_'); 
            end

    end
    
end

% --- Retrieve current tool
this.tool = ctool;