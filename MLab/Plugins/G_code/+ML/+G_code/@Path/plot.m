function out = plot(this, varargin)
%GEOM.POLY.PLOT plot a polygon
%*  GEOM.POLY.PLOT() plot the polygon.
%
%*  GEOM.POLY.PLOT(..., 'Property', PROPERTY) let you define a list of
%   properties which are similar to Matlab's plot function.
%
%*  H = GEOM.POLY.PLOT(...) return the plot handle.
%
%[  Note: the 'axes_handle' property cannot be specified.  ]
%
%*  See also: Geom.Poly, plot.

switch this.dim
    
    case 1
        
        h = plot(this.pos(:,1), zeros(this.N,1), varargin{:});
        
    case 2
        
        h = plot(this.pos(:,1), this.pos(:,2), varargin{:});
        
    case 3
        
        h = plot3(this.pos(:,1), this.pos(:,2), this.pos(:,3), varargin{:});
        
    otherwise
   
        error('POLY.PLOT:WrongDim', 'Plot cannot be generated for this dimension');
end

if nargout, out = h; end