% ML.HUI Example 1: Hello world
%
% This sample program opens a HUI window and display the famous 'Hello
% world !' message.
%
%   See also: ML.HUI.
% -------------------------------------------------------------------------

% Create ML.HUI object
H = ML.HUI('Example_1');

% Insert 'Hello world !' at position (1,1) in the grid
H.text([1 1], 'Hello world !');

% Display the HUI
H.show;