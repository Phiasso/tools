classdef HUI_Example_3<ML.HUI
%HUI_Example_3 is an example ML.HUI.
% This class is an eample of what can be achieved with the HUI class.
    
    % --- PROPERTIES ------------------------------------------------------
    properties
        
    end
    
    % --- METHODS ---------------------------------------------------------
    methods
        
        % _________________________________________________________________
        function this = HUI_Example_3(varargin)
        % Constructor

            % --- Call the parent's constructor
            this = this@ML.HUI(varargin{:});
        
        end
       
        % _________________________________________________________________
        function update_text(this, src)
        
            % Set the text widget
            this.set_widget('Text', 'text', ['You pressed the ' src.tag '.']);
            
        end
        
        % _________________________________________________________________
        function fun1(this, src)
            
            % Update the text
            this.update_text(src);
                  
            % Make the fist button disappear
            this.set_widget('Button 1', 'visible', false);
            
            % Make other buttons appear
            this.set_widget('Button 2', 'visible', true);
            this.set_widget('Button 3', 'visible', true);
            
            % Disable button 2
            this.set_widget('Button 2', 'disabled', true);
            this.set_widget('Button 2', 'text', 'I''m disabled');
            
            % Refresh display
            this.show;
            
        end
        
        % _________________________________________________________________
        function fun2(this, src)
            
            % Update the text
            this.update_text(src);
                  
            % Make the fist button appear
            this.set_widget('Button 1', 'visible', true);
            
            % Make other buttons disappear
            this.set_widget('Button 2', 'visible', false);
            this.set_widget('Button 3', 'visible', false);
            
            % Refresh display
            this.show;
            
        end
        
        % _________________________________________________________________
        function fun3(this, src)
            
            % Update the text
            this.update_text(src);
            
            % Enable button 2
            this.set_widget('Button 2', 'disabled', false);
            this.set_widget('Button 2', 'text', 'I''m enabled');
            
            % Make button 3 disappear
            this.set_widget('Button 3', 'visible', false);
            
            % Refresh display
            this.show;
            
        end
        
    end
end
