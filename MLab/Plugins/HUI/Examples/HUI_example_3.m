% ML.HUI Example 3: Buttons and action
%
% Three buttons allow you to trig different actions.
%
%   See also: ML.HUI.
% -------------------------------------------------------------------------

% Create ML.HUI object
H = HUI_Example_3('Example_3');

H.text([1 1], 'This text will be replaced soon.', 'tag', 'Text');

L = H.button([2 1], 'Click here', 'tag', 'Button 1', 'click', @fun1);
L = H.button([2 2], '', 'tag', 'Button 2', 'click', @fun2, 'visible', false);
L = H.button([2 3], 'Enable', 'tag', 'Button 3', 'click', @fun3, 'visible', false);

% Set cell options
H.cell_options([2 1], 'width', '33%');
H.cell_options([2 2], 'width', '33%');

% Display the HUI
H.show;