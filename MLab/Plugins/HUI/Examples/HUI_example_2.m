% ML.HUI Example 2: Tabs
%
% This sample program opens a HUI window and display different content in
% two different tabs.
%
%   See also: ML.HUI.
% -------------------------------------------------------------------------

% Create ML.HUI object
H = ML.HUI('Example_2');

% Define First tab
t1 = H.new_tab('First');
H.text([1 1], 'Text of the first tab.');

% Define Second tab
t2 = H.new_tab('Second');
H.text([1 1], 'Text of the second tab.');

% Set the focus on the first tab
H.display_tab = t1;

% Display the HUI
H.show;