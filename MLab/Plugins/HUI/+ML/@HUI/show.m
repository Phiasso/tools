function show(this, varargin)
%ML.HUI.show Display the HUI in the browser
%   ML.HUI.SHOW() builds and displays the HUI is the browser.
%
%   See also: ML.HUI, ML.HUI.callback.

% === Inputs ==============================================================

in = ML.Input;
in.mode{'no_grid'} = @ischar;
in = +in;

% =========================================================================

if this.grid_layout
    
    % === GRID LAYOUT MODE ================================================
    
    % --- Prepare html
    this.prepare_html;
    
    % --- Build virtual grid with widgets
    
    % Grid size
    nrows = max(cellfun(@(x) max(x.row), this.tabs(this.display_tab).widgets));
    ncols = max(cellfun(@(x) max(x.col), this.tabs(this.display_tab).widgets));
    
    % Grid content
    G = cellfun(@(x) '', cell(nrows, ncols), 'UniformOutput', false);
    
    for i = 1:numel(this.tabs(this.display_tab).widgets)
        
        % --- Get widget
        W = this.tabs(this.display_tab).widgets{i};
        
        % --- Append
        G{min(W.row), min(W.col)} = [G{min(W.row), min(W.col)} this.widget2html(W)];
        
        % --- Spanning options
        if numel(W.col)*numel(W.row)>1
            
            % Rows
            if numel(W.row)>1
                this.tabs(this.display_tab).layout(min(W.row), min(W.col)).options{end+1} = 'rowspan';
                this.tabs(this.display_tab).layout(min(W.row), min(W.col)).options{end+1} = num2str(numel(W.row));
            end
            
            % Cols
            if numel(W.col)>1
                this.tabs(this.display_tab).layout(min(W.row), min(W.col)).options{end+1} = 'colspan';
                this.tabs(this.display_tab).layout(min(W.row), min(W.col)).options{end+1} = num2str(numel(W.col));
            end
            
            % Cells to remove
            for j = W.row
                for k = W.col
                    if j~=min(W.row) || k~=min(W.col)
                        this.tabs(this.display_tab).layout(j,k).options{end+1} = 'REMOVE';
                    end
                end
            end
            
        end
        
    end
    
    % --- Grid display mode
    main = this.html.main();
    switch in.mode
        case 'grid'
            grid = this.html.table(main, 'class', 'grid_debug');
        otherwise
            grid = this.html.table(main, 'class', 'grid');
    end
    
    % --- Build html grid
    for i = 1:nrows
        tr = this.html.tr(grid);
        for j = 1:ncols
            
            % --- Options
            options = this.tabs(this.display_tab).layout(i,j).options;
            if isempty(options), options = {}; end
            
            % --- Insert content
            if ~ismember('REMOVE', options)
                this.html.td(tr, G{i,j}, options{:});
            end
            
        end
    end
    
    % --- Build and show HTML
    this.html.show;
    
else
    
    % === FREE HTML MODE ==================================================
   
    % -- Build html
    h = this.html.build;
    
    % --- Insert widgets
   	for i = 1:numel(this.tabs(this.display_tab).widgets)
        
        % --- Get widget
        W = this.tabs(this.display_tab).widgets{i};
        
        % --- Insert
        h = strrep(h, ['<[' W.tag ']>'], this.widget2html(W));
        
    end
    
    % --- Show html 
    this.html.show(h);
end