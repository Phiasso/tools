function cell_options(this, varargin)
%ML.HUI.cell_options
%
%   See also: ML.HUI.

% === Inputs ==============================================================

in = ML.Input;

in.row = @isnumeric;
in.col = @isnumeric;
in.tab(this.current_tab) = @isnumeric;

[in, notin] = +in;

% =========================================================================

this.tabs(in.tab).layout(in.row, in.col).options = notin;