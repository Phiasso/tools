classdef HUI<handle
%ML.HUI [HUI plugin] The MLab Html User Interface class.
% The 'name' property should be a value structure field 
    
    % --- PROPERTIES ------------------------------------------------------
    properties (Access = private)
        
        grid_layout = true;
        tabs = struct('name', {}, 'widgets', {}, 'layout', struct('options', {}));
        
    end
    
    properties (GetAccess = public, SetAccess = private)
        
        name = '';
        
    end
    
    properties (Access = public)
        
        html = ML.HTML;
        style = 'classic';
        current_tab = 1;
        display_tab = 1;
        
    end
    
    % --- METHODS ---------------------------------------------------------
    methods
        
        % _________________________________________________________________
        function this = HUI(varargin)
        %ML.HUI Constructor
            
            % === Inputs ==================================================
            
            in = ML.Input;
            in.name{ML.uniqid} = @ischar;
            in.grid(true) = @islogical;
            in = +in;
            
            % =============================================================
        
            % --- Get and check the name
            
            % !! CHECK not already assigned
            this.name = in.name;
            
            % -- Declare the HUI
            ML.HUI.manager('set', this);
            
            % --- First tab, empty
            this.new_tab('');
            
            % --- Grid layout ?
            this.grid_layout = in.grid;
            
            if ~this.grid_layout, this.prepare_html; end

        end

    end
    
end
