function out = get(this, varargin)
%ML.GUI.get
%
%   See also: ML.GUI.

% === Inputs ==============================================================

in = ML.Input;

in.src = @(x) isstruct(x) || ischar(x);
in.key{'Value'} = @ischar;
in = +in;

% =========================================================================

% --- Default tab
if ischar(in.src)
    in.src = struct('tab', this.display_tab, 'tag', in.src);
end

% --- Get object widget property
out = get(findobj('Tag', in.src.tag), in.key);
