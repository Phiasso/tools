function tag = edit(this, varargin)
%ML.GUI.edit Edit shortcut
%
%   See also: ML.HUI.

% === Inputs ==============================================================

in = ML.Input;

% --- Required
if this.grid_layout
    in.row = @isnumeric;
    in.col = @isnumeric;
else
    in.pos = @isnumeric;
end
in.String = @(x) ischar(x) || iscellstr(x);
in.Callback = @ML.isfunction_handle;

% --- Param / value pairs
in.Tag('') = @ischar;
in.BackgroundColor([1 1 1]) = @isnumeric;
in.tab(this.current_tab) = @isnumeric;
if this.grid_layout
    in.rowspan(1) = @isnumeric;
    in.colspan(1) = @isnumeric;
    in.halign('center') = @(x) ismember(x, {'left', 'center', 'right'});
    in.valign('center') = @(x) ismember(x, {'bottom', 'center', 'top'});
end

[in, notin] = +in;

% =========================================================================

% --- Parameters for the figure widget 
in.params = {'Style', 'edit'};
in.params = [in.params {'Tag', in.Tag}];
in.params = [in.params {'String', in.String}];
in.params = [in.params {'Callback', @(hObject, callbackdata) this.(func2str(in.Callback))}];
in.params = [in.params {'BackgroundColor', 'w'}];
in.params = [in.params notin'];

% --- Remove useless fields
in = rmfield(in, 'Tag');
in = rmfield(in, 'String');
in = rmfield(in, 'Callback');
in = rmfield(in, 'BackgroundColor');

% --- Define figure widget
tmp = this.def_widget(in);

% --- Output
if nargout
    tag = tmp;
end