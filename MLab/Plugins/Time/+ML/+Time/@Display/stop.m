function stop(this, text)
%ML.Timer.stop Stop Timer
%   ML.TIMER.STOP() stops the Timer.
%
%   ML.TIMER.STOP(TXT) stops the Timer and display the text TXT.
%
%   See also: ML.Timer.start, ML.Timer.step.

if ~isnan(this.t0)
    fprintf(' %.02f sec\n', toc(this.t0));
end

% --- Text display
if exist('txt', 'var')
    fprintf('%s\n', text);
end

% --- Reset timer
this.t0 = NaN;