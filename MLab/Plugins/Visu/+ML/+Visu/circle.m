function out = circle(pos, param, varargin)
%ML.Visu.circle Plot circle arcs
%	ML.VISU.CIRCLE(XY, R) plots N circles with radius R at the positions 
%   defined the N-by-2 array of positions XY. R can be either a single 
%   value or a N-by-1 vector of radii.
%
%   ML.VISU.CIRCLE(XY, ABT) where ABT is a 1-by-3 or N-by-3 array, draw
%   ellipses whose major axis is A, minor axis is B and angle is T
%   (in radian).
%
%   ML.VISU.CIRCLE(..., 'arc', [A1 A2]) draws arcs whose angles are comprised 
%   between A1 and A2.
%
%   ML.VISU.CIRCLE(..., 'number', NE) specifies the number of edges to 
%   describe the circles. The default value is 60.
%
%   ML.VISU.CIRCLE(..., 'Propertyname', property) adds properties which are
%   the same than for the plot function.
%
%   H = ML.VISU.CIRCLE(...) returns a cell of the corresponding handles.
%
%*  See also: ML.Visu.ellipse, ML.Visu.disk.

% === Input variables =====================================================

in = inputParser;
in.addRequired('pos', @isnumeric);
in.addRequired('param', @isnumeric);
in.addParamValue('number', 60, @(n) n>=3 && mod(n,1)==0);
in.addParamValue('arc', [0 2*pi], @isnumeric);

key = {'number', 'arc'};
sub = {};
out = {};
for i = 1:2:numel(varargin)
    if ischar(varargin{i}) && ismember(lower(varargin(i)), lower(key)) && numel(varargin)>i
        sub = [sub varargin(i:i+1)];
    else
        out = [out varargin(i:i+1)];
    end
end
varargin = out;
vin = sub;

in.parse(pos, param, vin{:});
in = in.Results;

% =========================================================================

% --- Preparation
N = size(in.pos,1);
x = in.pos(:,1);
y = in.pos(:,2);
if size(in.param,1)==1, in.param = in.param*ones(N,1); end

% --- Processing
if size(in.arc,1)==1
    t = ones(N,1)*linspace(in.arc(1), in.arc(2), in.number+1);
else
    t = NaN(0, in.number+1);
    for i = 1:N
        t(i,:) = linspace(in.arc(i,1), in.arc(i,2), in.number+1);
    end
end

if size(in.param, 2)==1     % Circles
    
    r = in.param*ones(1, in.number+1);
    X = x*ones(1,in.number+1) + r.*cos(t);
    Y = y*ones(1,in.number+1) + r.*sin(t);
    
elseif size(in.param, 2)==3 % Ellipses
    
    a = in.param(:,1)*ones(1, in.number+1);
    b = in.param(:,2)*ones(1, in.number+1);
    theta = in.param(:,3)*ones(1, in.number+1);
    X = x*ones(1,in.number+1) + a.*cos(theta).*cos(t) - b.*sin(theta).*sin(t);
    Y = y*ones(1,in.number+1) + a.*sin(theta).*cos(t) + b.*cos(theta).*sin(t);
    
else
    error('VISU.CIRCLES: Wrong size for the second argument. Aborting.');
end

% --- Define properties
A = struct();
for i = 1:2:numel(varargin)
    key = varargin{i};
    if ischar(key) && i<numel(varargin)
        A.(Text.str2field(key)) = varargin{i+1};
    else
        warning('GET_ARGINS: Unrecognized field.');
    end
end

F = fields(A);
P = cell(N, 2*numel(F));
faster_method = true;

for i = 1:numel(F)
    
    if iscell(A.(F{i}))   
        
        faster_method = false;
        
        % Check number of elements
        if numel((A.(F{i})))==N
            
            for j = 1:N
                P{j, 2*i-1} = F{i};
                if strcmpi(F{i}, 'color')
                    P{j, 2*i} = Color(A.(F{i}){j});
                else
                    P{j, 2*i} = A.(F{i}){j};
                end
            end
        else
            error('VISU.CIRCLE:input', ['VISU.CIRCLE: Wrong number of values for the property ' F{i} '. ' num2str(N) ' expected and ' num2str(numel(A.(F{i}))) ' received. Aborting.']);
        end
        
    else
        for j = 1:N
            P{j, 2*i-1} = F{i};
            P{j, 2*i} = A.(F{i});
        end
    end
end

% --- Display

hold on

if faster_method

    % This method is roughly 2x faster sice there is just one call to the
    % plot function. However it can be used only when the properties of the
    % circles are all the same.
    
    h = plot(X',Y', varargin{:});
    if nargout
        out = mat2cell(h, ones(numel(h),1), 1); 
    end

else

    % This method has to be used when at least one of the properties is
    % distributed over the N circles.
    
    h = cell(N,1);
    for i = 1:N
        h{i} = plot(X(i,:), Y(i,:), P{i,:});
    end
    if nargout, out = h; end
    
end