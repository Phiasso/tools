function out = display(this, varargin)
%ML.Steps.display Display the current state in the current THF.
%   ML.STEPS.DISPLAY displays the current state of the Steps object.
%
%   See also ML.Steps.

% === Input variables =====================================================

in = inputParser;
in.addParamValue('format', 'html', @ischar);
in.addParamValue('empty', '&nbsp;', @ischar);

in.parse(varargin{:});
in = in.Results;

% =========================================================================

endl = char(10);

switch in.format
    
    case 'html'
        
        s = '<table cellspacing=0 cellpadding=5 style="border-bottom: 1px solid black; border-right: 1px solid black;">';
        for i = 0:numel(this.elms)
            s = [s '<tr>' endl];
            for j = 0:numel(this.steps)
                s = [s '<td style="border-top: 1px solid black; border-left: 1px solid black; text-align: center;">'];
                
                if i==0 && j==0, continue; end
                if i==0, s = [s this.steps{j}]; continue; end
                if j==0, s = [s this.elms{i}]; continue; end
                
                if isempty(this.status{i,j})
                    s = [s in.empty];
                else 
                    s = [s this.status{i,j}];
                end
                s = [s '</td>' endl];
            end
            s = [s '</tr>' endl];
        end
        s = [s '</table>' endl];
end

% --- Output
if nargout
    out = s;
end
