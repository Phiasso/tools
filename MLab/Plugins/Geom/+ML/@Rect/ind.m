function out = ind(this, W, H)
%ML.Rect.ind Index in a matrix
%   I = ML.IND(W, H) returns the indexes of the points inside the rectangle 
%   with integer coordinates. The indexes are relative to an array of size
%   W-by-H.
%
%   I = ML.IND(WH) accepts a 2 element vector WH.
%
%   See also ML.Rect.

% --- Inputs
if ~exist('H', 'var')
    H = W(2);
    W = W(1);
end

% --- Ooutput
X = ceil(this.x1):floor(this.x2);
Y = ceil(this.y1):floor(this.y2);
[I, J] = meshgrid(X, Y);
out = sub2ind([H W], J, I);