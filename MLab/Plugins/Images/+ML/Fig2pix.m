function out = Fig2pix(varargin)
%ML.Fig2pix Converts a figure to pixels
%
%   See also: .

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addParamValue('fig', gcf, @isnumeric);
in.addParamValue('width', 1000, @isnumeric);
in.addParamValue('height', 750, @isnumeric);
in.addParamValue('res', 150, @isnumeric);
in.addParamValue('factor', 1, @isnumeric);
in.addParamValue('method', 'bitmap', @ischar);
in = +in;

% =========================================================================

set(in.fig, 'PaperUnits', 'inches');
set(in.fig, 'PaperPosition', [0 0 in.width in.height]/in.res);

switch in.method
    
    case 'bitmap'
        
        fname = [tempname '.png'];
        print(in.fig, '-dpng', sprintf('-r%d',in.res), fname);
        tmp = imread(fname);
        out = double(tmp(1:in.height,1:in.width,:))/255;
        delete(fname);
        
    case 'vector'
        
        set(in.fig, 'PaperPosition', [0 0 1 in.height/in.width]*6/in.factor);
        
        fname_eps = [tempname '.eps'];
        fname_png = [tempname '.png'];
        print(in.fig, '-depsc2', fname_eps);
        unix(['inkscape -f "' fname_eps '" -w ' num2str(in.width) ' -h ' num2str(in.height) ' -e "' fname_png '"']);
        
        tmp = imread(fname_png);
        out = double(tmp(1:in.height,1:in.width,:))/255;
        
        delete(fname_eps);
        delete(fname_png);
        
end