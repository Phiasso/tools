function P = pdf2(varargin)
%ML.Stat.pdf2 2D Probability density function
%   P = ML.Stat.pdf2(XBINS, YBINS, VALUES) computes the 2D-pdf of the 
%   n-by-2 array VALUES, over the  bins edges XBINS and YBINS. XBINS and 
%   YBINS can be either one dimensional binning vectors or single numbers
%   representing the number of bins. P is a structure containing the 
%   following fields: 
%       - 'xbin'
%       - 'ybin'
%       - 'hist'
%       - 'pdf'
%
%   ML.Stat.pdf2(..., 'binning', BTYPE) authorizes for logarithmic binning.
%   BTYPE is a 2-elements cell which can be either 'lin' or 'log'. The 
%   default value is {'lin', 'lin'}.
%
%   ML.Stat.pdf2(..., 'quiet', true) displays no warning.
%
%   See also ML.Stat.pdf, ML.Stat.kde2
%
%   More on <a href="matlab:ML.doc('ML.Stat.pdf2');">ML.doc</a>

% --- Inputs
in = ML.Input;
in.xbin = @isnumeric;
in.ybin = @isnumeric;
in.val = @isnumeric;
in.binning({'lin' 'lin'}) = @iscellstr;
in.quiet(false) = @islogical;
in = +in;

% --- Automatic binning mode
if numel(in.xbin)==1

    % Default value
    if (in.xbin<=0 || isnan(in.xbin)), in.xbin = 97; end
    
    switch in.binning{1}
        case 'lin'
            in.xbin = linspace(nanmin(in.val(:,1)), nanmax(in.val(:,1)), in.xbin+1);
        case 'log'
            in.xbin = logspace(log10(nanmin(in.val(:,1))), log10(nanmax(in.val(:,1))), in.xbin+1);
    end
end

if numel(in.ybin)==1
    
    % Default value
    if (in.ybin<=0 || isnan(in.ybin)), in.ybin = 97; end

    switch in.binning{2}
        case 'lin'
            in.ybin = linspace(nanmin(in.val(:,2)), nanmax(in.val(:,2)), in.ybin+1);
        case 'log'
            in.ybin = logspace(log10(nanmin(in.val(:,2))), log10(nanmax(in.val(:,2))), in.ybin+1);
    end
end

% --- Ensure binning increase
in.xbin = sort(in.xbin(:));
in.ybin = sort(in.ybin(:));

if size(in.val,2)~=2, 
    if size(in.val,1)==2
        in.val = in.val'; 
    else
        error('PDF2: The array of values is not of size n-by-2.'); 
    end;
end

% --- Checks
if ~isprime(numel(in.xbin)) && ~in.quiet
    fprintf('Warning: The number of x-bins (%i) is not a prime number.\n', numel(in.xbin));
end
if ~isprime(numel(in.ybin)) && ~in.quiet
    fprintf('Warning: The number of y-bins (%i) is not a prime number.\n', numel(in.ybin));
end

% --- Process

% Histogram
h = hist3(in.val,'Edges', {in.xbin, in.ybin});
h = [h(1:end-2, :) ; sum(h(end-1:end, :),1)];
h = [h(:, 1:end-2) ,  sum(h(:, end-1:end),2)];

[DX, DY] = meshgrid(diff(in.xbin), diff(in.ybin));
k = h./DX'./DY';

% Binning
xbin = [];
ybin = [];
switch in.binning{1}
    case 'lin'
        for i = 1:numel(in.xbin)-1
            xbin(i) = (in.xbin(i) + in.xbin(i+1))/2;
        end

    case 'log'
        for i = 1:numel(in.xbin)-1
            xbin(i) = sqrt(in.xbin(i)*in.xbin(i+1));
        end
end

switch in.binning{2}
    case 'lin'
        for i = 1:numel(in.ybin)-1
            ybin(i) = (in.ybin(i) + in.ybin(i+1))/2;
        end

    case 'log'
        for i = 1:numel(in.ybin)-1
            ybin(i) = sqrt(in.ybin(i)*in.ybin(i+1));
        end
end

% Integral
I = trapz(ybin, trapz(xbin, k));

% --- Output
P = struct('xbin', xbin, ...
           'ybin', ybin, ...
           'hist', h', ...
           'pdf', (k./I)');

       
%! ------------------------------------------------------------------------
%! Author: Raphaël Candelier
%! Version: 1.1
%
%! Revisions
%   1.1     (2015/04/07): Revised version (without 'vect' and 'trapz2'),
%               created help.
%   1.0     (2009/01/01): Initial version.
%
%! To_do
%   MLdoc
%! ------------------------------------------------------------------------
%! Doc
%   <title>To do</title>
