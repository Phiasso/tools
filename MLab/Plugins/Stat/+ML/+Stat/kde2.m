function P = kde2(varargin)
%ML.Stat.kde2 2D Kernel density estimator
%   P = ML.Stat.kde2(XBIN, YBIN, VALUES) returns the 2D kernel density 
%   estimation of VALUES over XBINS and YBINS. The output P is a structure
%   with fields:
%       - 'xbin'
%       - 'ybin'
%       - 'hist'
%       - 'pdf'
%
%   ML.Stat.kde2(..., 'kernel', K, 'param', P) specifies the type of kernel
%   and the associated parameters. The possible combinations are:
%       - K = 'Gaussian' (default): Gaussian kernel. P should be of the 
%           form [sigma_x sigma_y], or simply [sigma] if sigma_x = sigma_y.
%
%   See also ML.Stat.pdf2, ML.Stat.kde
%
%   More on <a href="matlab:ML.doc('ML.Stat.kde2');">ML.doc</a>

% === Input variables =====================================================

in = ML.Input;
in.xbin = @isnumeric;
in.ybin = @isnumeric;
in.val = @isnumeric;
in.kernel('Gaussian') = @(x) any(strcmpi(x, {'Gaussian'}));
in.param([]) = @isnumeric;
in = +in;

% =========================================================================

% --- Prepare output
P = struct('xbin', in.xbin(:)', ...
           'ybin', in.ybin(:)', ...
           'hist', zeros(numel(in.ybin), numel(in.xbin)), ...
           'pdf', []);
       
switch lower(in.kernel)
            
    case 'gaussian'
        
        % --- Get parameters
        switch numel(in.param)
            case 1
                sigma_x = in.param(1);
                sigma_y = in.param(1);
            case 2
                sigma_x = in.param(1);
                sigma_y = in.param(2);
            otherwise
                warning('ML.Stat.kde2::NumParam', ['Wrong number of parameters (' num2str(numel(in.param)) ').']);
                return
        end
                
        % --- Computation
        try
    
            X = permute(exp(-(bsxfun(@minus, P.xbin, in.val(:,1)).^2)/2/sigma_x), [3 2 1]);
            Y = permute(exp(-(bsxfun(@minus, P.ybin, in.val(:,2)).^2)/2/sigma_y), [2 3 1]);
            P.hist = sum(bsxfun(@times, X, Y), 3);
            
        catch
            
            [X, Y] = meshgrid(P.xbin, P.ybin);
            for i = 1:size(in.val,1)
                P.hist = P.hist + exp(-((X-in.val(i,1)).^2)/2/sigma_x - ((Y-in.val(i,2)).^2)/2/sigma_y);
            end
            
        end
    
end

% --- Normalization
P.pdf = P.hist/trapz(P.xbin, trapz(P.ybin, P.hist));

%! ------------------------------------------------------------------------
%! Author: Raphaël Candelier
%! Version: 1.1
%
%! Revisions
%   1.1     (2015/04/07): Revised version (without 'vect' and 'trapz2'),
%               created help.
%   1.0     (2009/01/01): Initial version.
%
%! To_do
%   MLdoc
%! ------------------------------------------------------------------------
%! Doc
%   <title>To do</title>
