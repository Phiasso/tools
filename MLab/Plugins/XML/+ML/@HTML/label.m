function [out1, out2] = label(this, varargin)
%ML.HTML.label Add a <label> element to the HTML object.
%   ML.HTML.LABEL() add a <label> element to the HTML object.
%
%   ML.HTML.LABEL(TEXT) specifies the text content of the element.
%
%   ML.HTML.LABEL(POSITION, TEXT) also specifies the position of the element.
%
%   ML.HTML.LABEL(..., 'opt', OPT, ...) specifies the element's options.
%
%   [ID, ID_TXT] = ML.HTML.LABEL(...) returns the identifiers of the new 
%   element an the text content.
%
%   See also ML.HTML.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.parent varargin];
end

in = ML.Input(varargin{:});
in.addRequired('position', @ML.isXMLpos);
in.addRequired('text', @ischar);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'tag', 'label', 'options', notin);
id_txt = this.add(id, 'text', in.text);

% --- Outputs
if nargout
    out1 = id;
    out2 = id_txt;
end