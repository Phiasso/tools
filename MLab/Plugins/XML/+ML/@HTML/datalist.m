function out = datalist(this, varargin)
%ML.HTML.datalist Add a <datalist> element to the HTML object.
%   ML.HTML.DATALIST() add a <datalist> element to the HTML object.
%
%   ML.HTML.DATALIST(POSITION) specifies the position of the element. The 
%   default position is given by the object property 'parent'.
%
%   ML.HTML.DATALIST(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.DATALIST(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'datalist', 'options', notin);

% --- Output
if nargout
    out = id;
end