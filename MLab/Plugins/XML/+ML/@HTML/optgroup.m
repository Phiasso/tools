function out = optgroup(this, varargin)
%ML.HTML.optgroup Add a <optgroup> element to the HTML object.
%   ML.HTML.OPTGROUP() add a <optgroup> element to the HTML object.
%
%   ML.HTML.OPTGROUP(LABEL) specifies the LABEL property of the element.
%
%   ML.HTML.OPTGROUP(POSITION, LABEL) also specifies the position of the 
%   element.
%
%   ML.HTML.OPTGROUP(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.OPTGROUP(...) returns the identifier of the new element.
%
%   See also ML.HTML, ML.HTML.select.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.parent varargin];
end

in = ML.Input(varargin{:});
in.addRequired('position', @ML.isXMLpos);
in.addRequired('label', @ischar);
[in, notin] = +in;

% =========================================================================

notin = ['label' ; in.label; notin];
id = this.add(in.position, 'cont', 'optgroup', 'options', notin);

% --- Outputs
if nargout
    out = id;
end