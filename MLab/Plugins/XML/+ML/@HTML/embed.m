function out = embed(this, varargin)
%ML.HTML.embed Add an <embed> element to the HTML object.
%   ML.HTML.EMBED() add an <embed> element to the HTML object.
%
%   ML.HTML.EMBED(SOURCE) specifies the source of the element.
%
%   ML.HTML.EMBED(POSITION, SOURCE) also specifies the position of the 
%   element.
%
%   ML.HTML.EMBED(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.EMBED(...) returns the identifiers of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.parent varargin];
end

in = ML.Input(varargin{:});
in.addRequired('position', @ML.isXMLpos);
in.addRequired('src', @ischar);
[in, notin] = +in;

% =========================================================================

notin = ['src' ; in.src; notin];
id = this.add(in.position, 'utag', 'embed', 'options', notin);

% --- Outputs
if nargout
    out = id;
end