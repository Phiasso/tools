function out = dl(this, varargin)
%ML.HTML.dl Add a <dl> element to the HTML object.
%   ML.HTML.DL() add a <dl> element to the HTML object.
%
%   ML.HTML.DL(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.DL(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.DL(...) returns the identifier of the new element.
%
%   See also ML.HTML, ML.HTML.dd, ML.HTML.dt.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'dl', 'options', notin);

% --- Output
if nargout
    out = id;
end