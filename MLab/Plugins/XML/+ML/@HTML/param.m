function out = param(this, varargin)
%ML.HTML.param Add an <param> element to the HTML object.
%   ML.HTML.PARAM() add an <param> element to the HTML object.
%
%   ML.HTML.PARAM(POSITION) specifies the position of the element. The 
%   default position is given by the object property 'parent'.
%
%   ML.HTML.PARAM(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.PARAM(...) returns the identifiers of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'utag', 'param', 'options', notin);

% --- Outputs
if nargout
    out = id;
end