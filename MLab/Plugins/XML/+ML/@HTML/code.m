function [out1, out2] = code(this, varargin)
%ML.HTML.code Add a <code> element to the HTML object.
%   ML.HTML.CODE() add a <code> element to the HTML object.
%
%   ML.HTML.CODE(TEXT) specifies the text content of the element.
%
%   ML.HTML.CODE(POSITION, TEXT) also specifies the position of the element.
%
%   ML.HTML.CODE(..., 'opt', OPT, ...) specifies the element's options.
%
%   [ID, ID_TXT] = ML.HTML.CODE(...) returns the identifiers of the new 
%   element an the text content.
%
%   See also ML.HTML.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.parent varargin];
end

in = ML.Input(varargin{:});
in.addRequired('position', @ML.isXMLpos);
in.addRequired('text', @ischar);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'tag', 'code', 'options', notin);
id_txt = this.add(id, 'text', in.text);

% --- Outputs
if nargout
    out1 = id;
    out2 = id_txt;
end