function out = tr(this, varargin)
%ML.HTML.tr Add a <tr> element to the HTML object.
%   ML.HTML.TR() add a <tr> element to the HTML object.
%
%   ML.HTML.TR(POSITION) specifies the position of the element. 
%   The default position is given by the object property 'parent'.
%
%   ML.HTML.TR(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.TR(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'tr', 'options', notin);

% --- Output
if nargout
    out = id;
end