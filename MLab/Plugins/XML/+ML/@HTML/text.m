function out = text(this, varargin)
%ML.HTML.text Add text to the HTML object.
%   ML.HTML.TEXT(T) adds some text T to the HTML object.
%
%   ML.HTML.TEXT(POSITION, T) specifies the position of the element. The 
%   default position is given by the HTML object property 'parent'.
%
%   ID = ML.HTML.TEXT(...) returns the identifier of the text.
%
%   See also ML.HTML.

% === Inputs ==============================================================

if ~ML.isXMLpos(varargin{1})
    varargin = [this.parent varargin];
end

in = ML.Input(varargin{:});
in.addRequired('position', @ML.isXMLpos);
in.addRequired('text', @ischar);
in = +in;

% =========================================================================

id = this.add(in.position, 'text', in.text);

% --- Output
if nargout
    out = id;
end