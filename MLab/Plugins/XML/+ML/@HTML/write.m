function fname = write(this, varargin)
%ML.HTML.show [HTML plugin] Show the HTML document
%   ML.HTML.SHOW() builds the HTML tree and displays it in the browser.
%
%   ML.HTML.BUILD(HTML) shows the HTML code in the browwser.
%
%   See also ML.HTML, ML.XML.build.

% === Inputs ==============================================================

in = ML.Input;
in.html{this.build} = @ischar;
in = +in;

% =========================================================================

% Double the % signs
html = strrep(in.html, '%', '%%');

% Write to temporary file
fid = fopen(this.filename, 'w');
fprintf(fid, html);
fclose(fid);

% Output
if nargout
    fname = this.filename;
end