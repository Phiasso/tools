function out = css(this, varargin)
%ML.HTML.css Add a CSS element to the HTML object.
%   ML.HTML.CSS(SOURCE) add a CSS element to the HTML object. SOURCE is
%   filename of the specific css style sheet. The css element is positionned 
%   in the header.
%
%   ML.HTML.CSS(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.CSS(...) returns the identifier of the new element.
%
%   See also ML.HTML, ML.HTML.style.

% === Inputs ==============================================================

in = ML.Input;
in.src = @ischar;
[in, notin] = +in;

% =========================================================================

options = ['rel'; 'stylesheet'; 'type'; 'text/css'; 'href'; in.src; notin];

id = this.add(this.head, 'tag', 'link', 'options', options);

% --- Outputs
if nargout
    out = id;
end
