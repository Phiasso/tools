function out = progress(this, varargin)
%ML.HTML.progress Add a <progress> element to the HTML object.
%   ML.HTML.PROGRESS() add a <progress> element to the HTML object.
%
%   ML.HTML.PROGRESS(POSITION) specifies the position of the element.
%
%   ML.HTML.PROGRESS(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.EM(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'tag', 'progress', 'options', notin);

% --- Outputs
if nargout
    out = id;
end