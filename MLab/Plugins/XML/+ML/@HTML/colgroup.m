function out = colgroup(this, varargin)
%ML.HTML.colgroup Add a <colgroup> element to the HTML object.
%   ML.HTML.COLGROUP() add a <colgroup> element to the HTML object.
%
%   ML.HTML.COLGROUP(POSITION) specifies the position of the element. The default
%   position is given by the object property 'parent'.
%
%   ML.HTML.COLGROUP(..., 'opt', OPT, ...) specifies the element's options.
%
%   ID = ML.HTML.COLGROUP(...) returns the identifier of the new element.
%
%   See also ML.HTML.

% === Inputs ==============================================================

in = ML.Input(varargin{:});
in.addOptional('position', this.parent, @ML.isXMLpos);
[in, notin] = +in;

% =========================================================================

id = this.add(in.position, 'cont', 'colgroup', 'options', notin);

% --- Output
if nargout
    out = id;
end