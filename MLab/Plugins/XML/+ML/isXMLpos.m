function out = isXMLpos(in)
%ML.XML.isXMLpos Check if the input is a valid XML position.
%   OUT = ML.XML.ISXMLPOS(IN) checks if the input is a valid XML position
%   and returns a boolean.
%
%   See also ML.XML.

if isnumeric(in)
    out = in>=0 && in==round(in);
elseif isstruct(in)
    f = fieldnames(in);
    out = all(ismember({'parent', 'pos'}, f));
else
    out = false;
end