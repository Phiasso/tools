function [p, stars] = multiwilcoxon(X, varargin)
% [p, stars] = MULTIWILCOXON(X)
%
% MULTIKSTEST wraps signigicance test ranksum function, computing a pvalue
% for each combination of columns. 
%
% INPUTS :
% ------
% X : array or cell containing samples on columns. NaN are considered as 
% missing values.
% Everything else is passed to the ranksum function.
%
% RETURNS :
% -------
% p : array containing p values for each combinations of columns.
% stars : string array containing number of corresponding stars.

[p, stars] = multitest(X, 'wilcoxon', varargin{:});