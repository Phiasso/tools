% Rename bunch of files

cwd = '/home/ljp/Science/Projects/Neurofish/Data/Thermotaxis/2018-11-16/Run 01/Images/';
pattern = 'Images';

files = dir([cwd '*tif']);

for idx = 1:numel(files)
    
    name = files(idx).name;
    
    ind = strfind(name, pattern);
    n_ind = numel(pattern);
    
    prefix = name(ind:ind + n_ind-1);
    number = name(ind + n_ind:end); 
    
    newprefix = [prefix '_'];
    nname = [newprefix number];
    
    oldname = [files(idx).folder filesep name];
    newname = [files(idx).folder filesep nname];
    rename_(oldname, newname)
    %unix(['mv "' oldname '" "' newname '"']);
    
end

function rename_(oldname, newname)
	java.io.File(oldname).renameTo(java.io.File(newname));
end