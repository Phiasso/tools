% Matlab startup file.
% Changes defaults values for axes object to get better plots by default.
% Adds some tools to the path and launch MLab projects manager.

% Add personnal tools
if isunix
    username = getenv('USER');
    base = [filesep 'home' filesep username filesep 'Documents' filesep 'MATLAB' filesep];
elseif ispc
    username = getenv('username');
    base = ['C:' filesep 'Users' filesep username filesep 'Documents' filesep 'MATLAB' filesep];
end
    
addpath(genpath(base));

% Change default lines values
set(groot, 'defaultLineLineWidth', 1.5);
set(groot, 'defaultErrorbarLineWidth', 1.5);

% Change default axes values
set(groot, 'defaultAxesFontName', 'Arial');
set(groot, 'defaultAxesFontSize', 12);
set(groot, 'defaultAxesXGrid', 'on');
set(groot, 'defaultAxesYGrid', 'on');
set(groot, 'defaultAxesPositionConstraint', 'innerposition');

% Change default figure values
set(groot, 'defaultFigureColor', [0.98 0.98 0.98]);
set(groot, 'defaultFigureWindowStyle', 'docked');

% Change default legend values
set(groot, 'defaultLegendColor', 'none');
set(groot, 'defaultLegendFontSize', 12);

% Launch MLab Projects manager
ML.Projects;

% Cleanup
clear;
