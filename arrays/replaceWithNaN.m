function out = replaceWithNaN(X, I, varargin)
% Replaces values indexed with I in X with NaNs, optionnaly with something
% else.
% Equivalent to X(I) = val, made to be used within cellfun.
%
% INPUTS :
% ------
% X : array where values are to be replaced
% I : index of values to be replaced
% val : replace NaN with anything else
%
% RETURN :
% ------
% out : X array with replaced values

% --- Check input
p = inputParser;
p.addRequired('X', @isnumeric);
p.addRequired('I', @(x) isnumeric(x)||islogical(x));
p.addOptional('val', NaN, @isscalar);
p.parse(X, I, varargin{:});

X = p.Results.X;
I = p.Results.I;
val = p.Results.val;

% --- Processing
out = X;
out(I) = val;