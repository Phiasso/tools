# Matlab Tools
This is a set of small tools to get some things done under MATLAB, such as beautiful plot (boxplot, shaded error bars), some statistical stuff (pdf, cdf, batch significance tests) and some extra.
The MLab folder contains the last version of MLab, a MATLAB plugin created by Raphaël Candelier (http://candelier.fr/MLab/) and has its own licence (GNU GPL v3).

# How to work with LightSheetAnalysis

## Architecture to respect
D:\Base\Data\Study\1999-02-30\Run 01
Parameters.txt
Images

## Main functions
`ML.start`

`ML.Projects`
select wanted project

edit and launch
`worksheet_pre_routine`

`Routines.Config`

`Routines.IP`

